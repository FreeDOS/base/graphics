# GRAPHICS

Allows PrtScr to print graphics screens. (CGA/EGA/VGA/MCGA, on PostScript, ESC/P Epson 8/24pin and HP PCL printers)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## GRAPHICS.LSM

<table>
<tr><td>title</td><td>GRAPHICS</td></tr>
<tr><td>version</td><td>2008-07-14a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-04-28</td></tr>
<tr><td>description</td><td>Allows PrtScr to print graphics screens</td></tr>
<tr><td>summary</td><td>Allows PrtScr to print graphics screens. (CGA/EGA/VGA/MCGA, on PostScript, ESC/P Epson 8/24pin and HP PCL printers)</td></tr>
<tr><td>keywords</td><td>prtscr graphics printer driver dos</td></tr>
<tr><td>author</td><td>Eric Auer &lt;e.auer -at- jpberlin.de&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer &lt;e.auer -at- jpberlin.de&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/graphics/</td></tr>
<tr><td>original&nbsp;site</td><td>http://ericauer.cosmodata.virtuaserver.com.br/soft/</td></tr>
<tr><td>platforms</td><td>DOS (Microsoft Visual C++ in C mode, Borland C++), FreeDOS, OpenWatcom, Linux (gcc),</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Graphics</td></tr>
</table>
